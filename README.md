# fluo-diffraction-service


## Description

This is a web service that:

- loads a CIF structure file
- computes an estimate of the diffractogram and the fluorescence spectrum
- returns data files and plots

Some structures may fail in computing the diffraction pattern. 
The fluorescence will then work when given the chemical formula.

## Installation

For the computational part, on Debian/Ubuntu (change `$version` to the current McXtrace one):
```
apt install mcxtrace-suite-ng libxrl-dev mcxtrace-tools-python-mxplot-matplotlib-$version cif2hkl
```

Then install the following packages for the server part:
```
apt install apache2 libapache2-mod-perl2 libnet-ldap-perl libsys-cpu-perl 
apt install libsys-cpuload-perl libproc-background-perl gnuplot
```

Then configure the web server:
```
sudo a2enmod cgi
sudo service apache2 restart
```

And finally copy the web service files:

1. copy the `cgi-bin/fluo_diffraction.pl` into e.g. `/usr/lib/cgi-bin`
2. copy the `html/fluo_diffraction.html` into e.g. `/var/www/html/`
3. optionally re-compile the `bin/Fluo_Diff` model with `cd bin; mxrun --mpi=2 -c -n0 Fluo_Diff.instr`
4. copy the `bin/Fluo_Diff.out` into e.g. `/usr/local/bin/`
5. create storage area for results `mkdir /var/www/html/data/diffraction`
6. change ownership `chown -R www-data /var/www/html/data/diffraction`

You should adapt the `fluo_diffraction.pl` configuration section (e.g. path to hold calculation results, any LDAP server).
```perl
my $www_root      = "/var/www/html";    # root of the web server
my $target        = "data/diffraction"; # the sub-directory on the server, shown also in URLs.
my $ldap_uri      = '';       # e.g. 'ldap://195.221.10.1' or 'ldap://example.com:389' or ''
my $ldap_base     = 'dc=EXP'; # e.g. 'dc=EXP' or 'dc=example,dc=com' or ''
my $service_max_load = 0.8;   # deny service when server load is above
my $service_mpi_nb= 4;        # max nb of cores to use with codes that use MPI.
my $executable    = "DISPLAY= HWLOC_COMPONENTS=-gl CIF2HKL=cif2hkl mpirun -n $service_mpi_nb /usr/local/bin/Fluo_Diff.out";    # the name of the executable
```

## Usage as a web service

Simply connect to:

- http://localhost/fluo_diffraction.html

All results are stored e.g. at:

- http://localhost/data/diffraction

![landing page](images/landing.png)

![results](images/results.png)

## Usage via an API call

It is possible to programmatically send a request for a computation with a command such as:
```
curl --trace-ascii LOGFILE -F user_id=farhie -F user_pw='xxxxxx' -F energy=16.99 -F 'dataset=@"/full/path/to/MnSi.cif"' -F dataset_chem='' https://data-analysis.synchrotron-soleil.fr/cgi-bin/fluo_diffraction.pl
```

The result is returned in `LOGFILE` (can be set as `/dev/stdout`), with lines (can span on two lines to be merged) at the end such as:
```
0000: Location: /data/diffraction/MnSi.cif-farhie-20231006-134908/
```

which indicates the location of the calculation on the server. Append this path `/data/diffraction/MnSi.cif-farhie-20231006-134908/` to the server, here `https://data-analysis.synchrotron-soleil.fr/`.

## Credits

Powered by <a href="https://mcxtrace.org/">McXtrace</a>. (c) Synchrotron SOLEIL, France. GPL3 license.
See https://gitlab.com/soleil-data-treatment/soleil-software-projects/fluo-diffraction-service

---
(c) 2023 E. Farhi - Synchrotron SOLEIL/GRADES
