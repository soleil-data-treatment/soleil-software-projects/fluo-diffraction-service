#!/usr/bin/perl -w

# E. Farhi (c) Synchrotron SOLEIL GPL3
# https://gitlab.com/soleil-data-treatment/soleil-software-projects/fluo-diffraction-service
use strict;
use CGI; # see https://metacpan.org/pod/CGI
use CGI::Carp qw ( fatalsToBrowser );
use File::Basename;
use Net::LDAP;          # libnet-ldap-perl          for ldap user check
use File::Copy;
use File::Path;
use Sys::CPU;           # libsys-cpu-perl           for CPU::cpu_count
use Sys::CpuLoad;       # libsys-cpuload-perl       for CpuLoad::load
use Proc::Background;   # libproc-background-perl   for Background->new

# ensure all fatals go to browser during debugging and set-up
# comment this BEGIN block out on production code for security
BEGIN {
    $|=1;
    use CGI::Carp('fatalsToBrowser');
}

# use https://perl.apache.org/docs/2.0/api/Apache2/RequestIO.html
# for flush with CGI
my $r = shift;
if (not $r or not $r->can("rflush")) {
  push @ARGV, $r; # put back into ARGV when not a RequestIO object
}

use constant IS_MOD_PERL => exists $ENV{'MOD_PERL'};
use constant IS_CGI      => IS_MOD_PERL || exists $ENV{'GATEWAY_INTERFACE'};

$CGI::POST_MAX        = 100*1024; # in bytes, default 100kB
$CGI::DISABLE_UPLOADS = 0;        # 1 disables uploads, 0 enables uploads

# see http://honglus.blogspot.com/2010/08/resolving-perl-cgi-buffering-issue.html
#CGI->nph(1);

# CONFIG: adapt values to your needs
my $www_root      = "/var/www/html";    # root of the web server
my $target        = "data/diffraction"; # the sub-directory on the server, shown also in URLs.
my $ldap_uri      = '';       # e.g. 'ldap://195.221.10.1' or 'ldap://example.com:389' or ''
my $ldap_base     = 'dc=EXP'; # e.g. 'dc=EXP' or 'dc=example,dc=com' or ''
my $service_max_load = 0.8;   # deny service when server load is above
my $service_mpi_nb= 4;        # max nb of cores to use with codes that use MPI.
my $executable    = "DISPLAY= HWLOC_COMPONENTS=-gl CIF2HKL=cif2hkl mpirun -n $service_mpi_nb /usr/local/bin/Fluo_Diff.out";    # the name of the executable

# get the form
my $form          = new CGI;
my $error         = "";

if (Sys::CpuLoad::load() / Sys::CPU::cpu_count() > $service_max_load) {
    $error .= "Server load exceeded. Try again later. ";
  }

# test form consistency
my @fields   = ( 'user_id','energy','dataset' ,'dataset_chem');

# test if the form has the required fields
for (@fields) {
  if ( not defined $form->param($_) ) {
    $error .= "Incomplete form: fields are missing: $_ ";
  }
}
# test if required information is there
for ('user_id') {
  if (not $form->param($_)) {
    $error .= "Some required (*) information is missing: $_ ";
  }
}
# test for valid credentials (when LDAP is defined)
my $authenticated = session_check_ldap(
                      $ldap_uri, $ldap_base,
                      $form->param('user_id'), $form->param('user_pw'));
if (index($authenticated, "FAILED") > -1) {
  $error .= $authenticated;
}

# get a valid dataset_name (will be a directory)
my $safe_filename_characters    = "a-zA-Z0-9_.-";
my $safe_chem_formula           = "A-Za-z0-9()[]";
my $user_id      = $form->param('user_id');
my $dataset      = $form->param('dataset');
my $dataset_chem = $form->param('dataset_chem');
my $energy       = $form->param('energy');
my $io_handle    = $form->upload('dataset');
my $dataset_name = $dataset;
$dataset_name    =~ s/[^$safe_filename_characters]//g; # remove illegal characters
$user_id         =~ s/[^$safe_filename_characters]//g; # remove illegal characters
$dataset_chem    =~ s/[^$safe_chem_formula]//g;        # remove illegal characters

my $servername    = $form->server_name();
if ($servername =~ "::1") { $servername = "localhost"; }

# we create a target directory: $dataset_name_$calculator_$user_id_$date
my( $sec, $min, $hour, $mday, $mon, $year ) = localtime;
my $datestamp = sprintf "%04d%02d%02d-%02d%02d%02d",
    $year+1900, $mon+1, $mday, $hour, $min, $sec;

my $directory;
if ($dataset_chem) {
  $directory = "$dataset_chem-$user_id-$datestamp";
} else {
  $directory = "$dataset_name-$user_id-$datestamp";
}

# we copy the uploaded file into the target
if (not $error) {
  if (defined $io_handle) {
    File::Path::make_path("$www_root/$target/$directory");  # make sure target exists
    # move the temp file to the storage we use
    if (not copy($form->tmpFileName( $io_handle ), "$www_root/$target/$directory/$dataset_name")) {
      $error .= "Can not copy $dataset into $directory. ";
    }
  } elsif ($dataset_chem) {
    # we are using fluorescence mode only
    File::Path::make_path("$www_root/$target/$directory");  # make sure target exists
  } else {
    $error .= "Did you forget to provide a material structure file ? Can not copy $dataset into $directory (no handle). ";
  }
}

# we create a configuration file to store settings
my $url = $ENV{'HTTP_ORIGIN'} . "/$target/$directory";

my $yaml = "# YAML\n";
$yaml .= "generated_by: $0 on " . localtime() . "\n";
$yaml .= "location: $url\n";
$yaml .= "date: " . localtime() . "\n";
$yaml .= "user_ip: " . $ENV{'REMOTE_HOST'} . " [" . $ENV{'REMOTE_ADDR'} . "]\n";
foreach my $f (@fields) {
  my $val = $form->param($f);
  if ($val) { $yaml .= "$f: $val\n"; }
  # if ($f != 'dataset' and $val) { $yaml .= "$f: $val\n"; }
}

if (open (FH, '>>', "$www_root/$target/$directory/config.yaml")) {
  print FH "$yaml\n";
  close(FH);
}

if (not $error) {
  # create the README file if it does not exist yet
  open (FH, '>>', "$www_root/$target/$directory/README.html") or die $!;
  print FH $form->header();
  close(FH);
  
  chmod 0644, "$www_root/$target/$directory/README.html";
}

# launch model in background
if (not $error) {
  my $cmd = "";
  # the -d directory must not exist
  if ($dataset_chem) {
    # fluo only
    $cmd = "$executable -d $www_root/$target/$directory/data -n 1e7 E0=$energy reflections=NULL chemical_formula=$dataset_chem >> $www_root/$target/$directory/fluo_diffraction-log.txt 2>&1";
  } else {
    # fluo and diffraction
    $cmd = "$executable -d $www_root/$target/$directory/data -n 1e7 E0=$energy reflections=$www_root/$target/$directory/$dataset_name chemical_formula=NULL >> $www_root/$target/$directory/fluo_diffraction-log.txt 2>&1";
  }
  print STDERR "$0: INFO: $cmd\n";
  
  my $proc = Proc::Background->new($cmd); 
  if (not $proc) {
    $error  .= "Could not start the computation for $dataset_name $dataset_chem";
  } else {
    # generate images. Must operate in the target directory. We use gnuplot.
    my $gnuplot = "# directory: $directory
# execute with: gnuplot -c <script>
set term svg
set autoscale fix

";
    if (not $dataset_chem) {
      # diffraction plots
      $gnuplot .= "set output '$www_root/$target/$directory/data/diffraction.svg'
set title 'Diffraction pattern $dataset_name E0=$energy [keV]'
set xlabel 'Angle [deg]'
set ylabel 'Intensity [p/s/bin]'
plot '$www_root/$target/$directory/data/diffraction.dat' using 1:2 with linespoints title 'Diffraction'

set logscale y
set ylabel 'Intensity [p/s/bin] (log-scale)'
set output '$www_root/$target/$directory/data/diffraction_log.svg'
replot

set view map
unset logscale
set logscale cb
set xlabel 'Angle [deg]'
set ylabel 'Height [cm]'
set output '$www_root/$target/$directory/data/diffraction_2d.svg'
plot '$www_root/$target/$directory/data/diffraction_2d.dat' matrix using 1:2:3 index 0 w image title 'Diffraction 2D'

";
    }
    # fluorescence plots
    $gnuplot .= "
set output '$www_root/$target/$directory/data/Fluorescence.svg'
set title  'Fluorescence $dataset_name E0=$energy [keV]'
set xlabel 'Energy [keV]'
set ylabel 'Intensity [p/s/bin]'
unset logscale
plot '$www_root/$target/$directory/data/Fluorescence.dat' using 1:2 with linespoints title 'Fluorescence'

set output '$www_root/$target/$directory/data/Fluorescence_all_log.svg'
set title  'Fluorescence/Compton/Rayleigh (log) $dataset_name E0=$energy [keV]'
set xlabel 'Energy [keV]'
set ylabel 'Intensity [p/s/bin]'
set logscale y
plot '$www_root/$target/$directory/data/Fluorescence_all.dat' using 1:2 with linespoints title 'Fluorescence/Compton/Rayleigh (log)'

set output '$www_root/$target/$directory/data/Fluorescence_log.svg'
set title  'Fluorescence $dataset_name E0=$energy [keV] (log)'
set xlabel 'Energy [keV]'
set ylabel 'Intensity [p/s/bin]'
set logscale y
plot '$www_root/$target/$directory/data/Fluorescence.dat' using 1:2 with linespoints title 'Fluorescence (log)'

set output '$www_root/$target/$directory/data/Compton.svg'
set title  'Compton $dataset_name E0=$energy [keV]'
set xlabel 'Energy [keV]'
set ylabel 'Intensity [p/s/bin]'
set logscale y
plot '$www_root/$target/$directory/data/Compton.dat' using 1:2 with linespoints title 'Compton'

set output '$www_root/$target/$directory/data/Rayleigh.svg'
set title  'Rayleigh $dataset_name E0=$energy [keV]'
set xlabel 'Energy [keV]'
set ylabel 'Intensity [p/s/bin]'
set logscale y
plot '$www_root/$target/$directory/data/Rayleigh.dat' using 1:2 with linespoints title 'Rayleigh'
";

    # write gnuplot script
    open (FH, '>>', "$www_root/$target/$directory/gnuplot.txt") or $error .= "Can not plot results in <a href='/$target/$directory/'>$directory<a>: $!";
    
    if (not $error) {
      print FH "$gnuplot\n";
      close(FH);
      # execute gnuplot to read results and creat plots
      $cmd = "gnuplot -c $www_root/$target/$directory/gnuplot.txt";
      # make sure files are written
      sleep 1 while ( $proc->alive && !(-e "$www_root/$target/$directory/data/Fluorescence.dat") );
      print STDERR "$0: INFO: $cmd\n";
      if (!(-e "$www_root/$target/$directory/data/Fluorescence.dat")) {
        $error .= "The calculation failed. Perhaps the CIF file has an issue ? all F^2=0 ?. ";
        $error .= "Have a look into <a href='/$target/$directory/'>fluo_diffraction-log.txt</a> for details.";
      } else {
        $proc = `$cmd`;
      }
    }
  }
} 

if (not $error) {
  # write the README.html file
  
  open (FH, '>>', "$www_root/$target/$directory/README.html") or die $!;
  print FH "<h1>Fluorescence/Diffraction for $dataset_name</h1>\n";
  print FH "<p>Stored in <a href='$url'>$url</a></p>\n";
  print FH "<p>Configuration:<br><pre>$yaml</pre></p>\n";
  print FH "<p>All data files are text-based, easy to import. </p>";
  
  # add plots
  if (not $dataset_chem) {
    print FH "<a href='/$target/$directory/data/diffraction.dat'><img src='/$target/$directory/data/diffraction.svg'></a><br>";
    print FH "<a href='/$target/$directory/data/diffraction.dat'><img src='/$target/$directory/data/diffraction_log.svg'></a><br>";
    print FH "<a href='/$target/$directory/data/diffraction_2d.dat'><img src='/$target/$directory/data/diffraction_2d.svg'></a><br>";
  }
  print FH "<a href='/$target/$directory/data/Fluorescence.dat'><img src='/$target/$directory/data/Fluorescence.svg'></a><br>";
  print FH "<a href='/$target/$directory/data/Fluorescence.dat'><img src='/$target/$directory/data/Fluorescence_log.svg'></a><br>";
  print FH "<a href='/$target/$directory/data/Fluorescence_all.dat'><img src='/$target/$directory/data/Fluorescence_all_log.svg'></a><br>";

  print FH "<a href='/$target/$directory/data/Compton.dat'><img src='/$target/$directory/data/Compton.svg'></a><br>";
  print FH "<a href='/$target/$directory/data/Rayleigh.dat'><img src='/$target/$directory/data/Rayleigh.svg'></a><br>";
  
  $url = $ENV{'HTTP_ORIGIN'} . "/$target";
  print FH "\n<hr><p>All data sets are available in <a href='$url'>$url</a></p>\n";
  print FH "</body></html>";
  close(FH);
  chmod 0644, "$www_root/$target/$directory/README.html";
  
  # finally redirect to the directory (with the README)
  my $url = $ENV{'HTTP_ORIGIN'} . "/$target/$directory/";
  print $form->redirect($url);
  # the 1st argument of CGI is an Apache RequestIO
  if (defined($r) and $r->can("rflush")) { 
    $r->rflush; 
  }
}
if ($error) {
  print $form->header ( ); # wrap up in html
  print "<h1>ERROR starting fluo/diffraction</h1>\n";
  print "<h2>\n";
  print "<p><div style='color:red'>$error</div></p>";
  print "</h2>\n";
  print "</body></html>";
}

# the 1st argument of CGI is an Apache RequestIO
if (defined($r) and $r->can("rflush")) { 
  $r->rflush; 
}

exit;



# ------------------------------------------------------------------------------
# session_check_ldap((ldap_server, ldap_base, user, password):
#   input:
#     ldap_server: an IP/URI        e.g. 'ldap://195.221.10.1:389'
#     ldap_base:   a search string, e.g. 'dc=EXP'
#     user:        a user name,     e.g. 'farhie'
#     password:    the user pw
#   return ""         when no check is done
#          "FAILED"   when authentication failed
#          "SUCCESS"  when authentication succeeded
sub session_check_ldap {
  my $ldap_server = shift;
  my $ldap_base   = shift;
  my $user        = shift;
  my $password    = shift;
  my $res = '';
  
  if (not $ldap_server or not $ldap_base) { return ""; }

  if (not $user) {
    return "FAILED: Missing Username.";
  }
  
  my $ldap = Net::LDAP->new($ldap_server)
    or return "FAILED: Cannot connect to LDAP server '$ldap_uri': $@";
    
  # identify the DN
  my $mesg = $ldap->search(
    base   => "$ldap_base",
    filter => "cn=$user", # may also be "uid=$user"
    attrs  => ['dn','mail','cn','uid']);
  
  if (not $mesg or not $mesg->count) {
    $res = "FAILED: Empty LDAP search.\n";
  } else {
    foreach my $entry ($mesg->all_entries) {
      my $dn    = $entry->dn();
      my $cn    = $entry->get_value('cn'); # may also be 'uid'
      my $bmesg = $ldap->bind($dn,password=>$password);
      if ( $bmesg and $bmesg->code() == 0 ) {
        $res = "SUCCESS: $user authenticated.";
      }
      else{
        my $error = $bmesg->error();
        $res = "FAILED: Wrong username/password (failed authentication): $error\n";
      }
    }
  }
  $ldap->unbind;
  return $res;
  
} # session_check_ldap

